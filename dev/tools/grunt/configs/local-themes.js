'use strict';
/**
 * Define Themes
 *
 * area: area, one of (frontend|adminhtml|doc),
 * name: theme name in format Vendor/theme-name,
 * locale: locale,
 * files: [
 * 'css/styles-m',
 * 'css/styles-l'
 * ],
 * dsl: dynamic stylesheet language (less|sass)
 *
 */
const themes = require('./themes');
const baseConfig = {
    area: 'frontend',
    locale: 'pt_BR',
    files: ['css/styles-m', 'css/styles-l'],
    dsl: 'less',
};
// Custom themes
themes['gk'] = {
    ...baseConfig,
    name: 'Odyssey/geek',
};
themes['gm'] = {
    ...baseConfig,
    name: 'Odyssey/games',
};
module.exports = themes;